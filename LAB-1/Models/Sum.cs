﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LAB_1.Models
{
    public class Sum
    {
        [Display(Name="Número 1")]
        public double Num1 { get; set; }
        [Display(Name = "Número 2")]
        public double Num2 { get; set; }
        [Display(Name = "Resultado")]
        public double Result => Num1 + Num2;
    }
}
