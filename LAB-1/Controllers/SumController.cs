﻿using LAB_1.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LAB_1.Controllers
{
    public class SumController : Controller
    {
        // GET: Sum
        public ActionResult Index()
        {
            return View();
        }

        // GET: Sum/Create
        [HttpPost]
        public ActionResult CalculateSum(Sum sum)
        {
            return View(nameof(Index), sum);
        }

        // POST: Sum/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Sum/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }
    }
}