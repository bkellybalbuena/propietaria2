﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Client.DateServiceReference {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://brayankelly.com/", ConfigurationName="DateServiceReference.DateServiceSoap")]
    public interface DateServiceSoap {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://brayankelly.com/GetCurrentDate", ReplyAction="*")]
        System.DateTime GetCurrentDate();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://brayankelly.com/GetCurrentDate", ReplyAction="*")]
        System.Threading.Tasks.Task<System.DateTime> GetCurrentDateAsync();
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface DateServiceSoapChannel : Client.DateServiceReference.DateServiceSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class DateServiceSoapClient : System.ServiceModel.ClientBase<Client.DateServiceReference.DateServiceSoap>, Client.DateServiceReference.DateServiceSoap {
        
        public DateServiceSoapClient() {
        }
        
        public DateServiceSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public DateServiceSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public DateServiceSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public DateServiceSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public System.DateTime GetCurrentDate() {
            return base.Channel.GetCurrentDate();
        }
        
        public System.Threading.Tasks.Task<System.DateTime> GetCurrentDateAsync() {
            return base.Channel.GetCurrentDateAsync();
        }
    }
}
