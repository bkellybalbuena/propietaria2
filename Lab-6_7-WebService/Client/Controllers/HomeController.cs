﻿using Client.DateServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Client.Controllers
{
    public class HomeController : Controller
    {
        private readonly DateServiceSoapClient dateServiceSoapClient;

        public HomeController()
        {
            dateServiceSoapClient = new DateServiceSoapClient();
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public async Task<ActionResult> GetCurrentDate()
        {
            var currentDate = await dateServiceSoapClient.GetCurrentDateAsync();

            ViewBag.CurrentDate = currentDate;

          return  View(nameof(Index));
        }
    } 
}