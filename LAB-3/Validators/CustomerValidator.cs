﻿using FluentValidation;
using LAB_3.Models;
using System.Text.RegularExpressions;

namespace LAB_3.Validators
{
    public class CustomerValidator: AbstractValidator<Customer>
    {
        private const int MAYOR_DE_EDAD = 18;
        private const string MAYOR_DE_EDAD_FAIL_MESSAGE = "Debe ser mayor de edad";
        public CustomerValidator()
        {
            RuleFor(c => c.FirstName).Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull().NotEmpty()
                .Must(e => Regex.IsMatch(e, "[a-zA-Z]+")).WithMessage("Please enter only letters");

            RuleFor(c => c.LastName).Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull().NotEmpty()
                .Must(e => Regex.IsMatch(e, "[a-zA-Z]+")).WithMessage("Please enter only letters");

            RuleFor(c => c.Age).Cascade(CascadeMode.StopOnFirstFailure).GreaterThanOrEqualTo(MAYOR_DE_EDAD)
                .WithMessage(MAYOR_DE_EDAD_FAIL_MESSAGE);

            RuleFor(c => c.Address).Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull().NotEmpty();
        }
    }
}
