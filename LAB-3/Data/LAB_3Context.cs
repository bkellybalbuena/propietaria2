﻿using Microsoft.EntityFrameworkCore;
using LAB_3.Models;

namespace LAB_3.Data
{
    public class LAB_3Context : DbContext
    {
        public LAB_3Context (DbContextOptions<LAB_3Context> options)
            : base(options)
        {
        }

        public DbSet<Customer> Customer { get; set; }
    }
}
