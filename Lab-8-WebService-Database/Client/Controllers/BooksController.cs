﻿
using System.Threading.Tasks;
using System.Net;

using System.Web.Mvc;
using Client.BooksServiceReference;
using System.Linq;
using System.Collections.Generic;

namespace Client.Controllers
{
    public class BooksController : Controller
    {

        private readonly BooksServiceSoapClient booksService;


        public BooksController()
        {
            booksService = new BooksServiceSoapClient();
        }
        // GET: Books
        public async Task<ActionResult> Index()
        {
            var response = await booksService.GetBooksAsync();
            var books = response.Body.GetBooksResult;
            return View(books);
        }

        // GET: Books/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var response = await booksService.FindByIdAsync(id.Value);
            var book =  response.Body.FindByIdResult;

            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // GET: Books/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Books/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Author,Price,PublishDate")] Book book)
        {
            if (ModelState.IsValid)
            {
               await booksService.CreateBookAsync(book);
                return RedirectToAction("Index");
            }

            return View(book);
        }

        // GET: Books/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var response = await booksService.FindByIdAsync(id.Value);
            var book = response.Body.FindByIdResult;

            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // POST: Books/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Author,Price,PublishDate")] Book book)
        {
            if (ModelState.IsValid)
            {
                await booksService.UpdateBookAsync(book);
                return RedirectToAction("Index");
            }
            return View(book);
        }

        // GET: Books/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var response = await booksService.FindByIdAsync(id.Value);
            var book = response.Body.FindByIdResult;

            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // POST: Books/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
             await booksService.DeleteByIdAsync(id);

            return RedirectToAction("Index");
        }
    }
}
