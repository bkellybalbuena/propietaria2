﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Services;
using WebService.Models;

namespace WebService.Services
{
    /// <summary>
    /// Summary description for BooksService
    /// </summary>
    [WebService(Namespace = "http://brayan-kelly.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]


    public class BooksService : System.Web.Services.WebService
    {
        private readonly BooksEntities booksContext;

        public BooksService()
        {
            booksContext = new BooksEntities();
        }

        [WebMethod]
        public Book[] GetBooks()
        {
            return  booksContext.Books.ToArray();
        }

        [WebMethod]
        public Book FindById(int id)
        {
            return  booksContext.Books.FirstOrDefault(b => b.Id == id);
        }

        [WebMethod]
        public void CreateBook(Book book)
        {
             booksContext.Books.Add(book);
            Save();
        }

        [WebMethod]
        public void UpdateBook(Book book)
        {
            var currentBook = booksContext.Books.FirstOrDefault(b => b.Id == book.Id);

            currentBook.Name = book.Name;
            currentBook.Price = book.Price;
            currentBook.PublishDate = book.PublishDate;

            booksContext.Entry(currentBook).State = EntityState.Modified;
            Save();
        }

        [WebMethod]
        public  void DeleteById(int id)
        {
            var book = booksContext.Books.FirstOrDefault(b => b.Id == id);

            if (book == null) throw new ArgumentException("Entity not founf");
             booksContext.Books.Remove(book);
             Save();

        }

        private void Save()
        {
            booksContext.SaveChanges();
        }
    }
}
